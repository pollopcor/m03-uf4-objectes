package Gos;

public class Gos {

	private String nom = "SenseNom";
	private int edat = 4;
	private char sexe = 'M';
	private int fills = 0;
	private static int comptador = 0;

	//
	// CONSTRUCTORS
	//
	
	// - Tots els valors
	public Gos(String nom, int edat, char sexe, int fills) {
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
		this.fills = fills;
		comptador++;
	}

	// - Per nom
	public Gos(String nom) {
		super();
		this.nom = nom;
		comptador++;
	}

	// - Copia
	public Gos(Gos gosOriginal) {
		this.nom = gosOriginal.nom;
		this.edat = gosOriginal.edat;
		comptador++;
	}

	// - Sense parametres
	public Gos() {

	}

	
	//
	// METODES
	//
	
	// - Bordar
	public void bordar() {
		System.out.println("guau guau");
	}

	// - Mostrar informacio
	@Override
	public String toString() {
		return "Nom: " + nom + " Edat: " + edat + " Sexe: " + sexe + " Fills: " + fills;
	}

	// - Copiar perro existente
	public static Gos clonar(Gos gosOriginal) {

		comptador++;
		Gos gosClonat = gosOriginal;
		return gosClonat;
	}

	// - Clonarse a si mismo
	public Gos clonar2() {
		comptador++;
		Gos GosClonat = new Gos();
		GosClonat.nom = nom;
		GosClonat.edat = edat;
		GosClonat.sexe = sexe;
		GosClonat.fills = fills;
		
		return GosClonat;
	}

	// - Numero de gossos creats
	public static void quantitatGossos() {
		System.out.println(comptador);
	}

	//
	// GETERS & SETTERS
	//
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}
	
}
