package Gos;

public class Principal {

	public static void main(String[] args) {

		Gos dalmata = new Gos("Jako", 5, 'M', 2); 	//Tots els parametres
		Gos bulldog = new Gos("Veno");			//Nomes amb el nom
		Gos terrier = new Gos(dalmata);			//Copiant el gos dalmata
		Gos pastorAlemany = Gos.clonar(dalmata);	//Clonar dalmata
		Gos cattledog = bulldog.clonar2();		//Clonar
		
		System.out.println(dalmata);
		System.out.println(bulldog);
		System.out.println(terrier);
		System.out.println(pastorAlemany);
		
		System.out.println("\n");
		
		dalmata.bordar();
		bulldog.bordar();
		terrier.bordar();
		pastorAlemany.bordar();
		
		Gos.quantitatGossos();
		
	}

}
