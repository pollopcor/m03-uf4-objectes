package introduccio;

public class Gos {

	private String nom;
	private String ra�a;
	private char genere;
	private int edat;
	private int pes;
	private boolean vacunat;
	private int chipID;

	//
	// SETTERS
	//
	
	// Nom
	public void setNom(String nom) {
		this.nom = nom;
	}

	// Ra�a
	public void setRa�a(String ra�a) {
		this.ra�a = ra�a;
	}

	// Genere
	public void setGenere(char genere) {
		if (genere == 'M' || genere == 'F')
			this.genere = genere;
	}

	// Edat
	public void setEdat(int edat) {
		if (0 > edat && edat < 30)
			this.edat = edat;
	}

	// Pes
	public void setPes(int pes) {
		if (pes > 0)
			this.pes = pes;
	}

	// Vacunat
	public void setVacunat() {
		if (!this.vacunat)
			this.vacunat = true;

	}

	// ID
	public void setChipID(int ID) {
		this.chipID = ID;
	}

	//
	// GETTERS
	//

	// Nom
	public String getNom() {
		return this.nom;
	}

	// Ra�a
	public String getRa�a() {
		return this.ra�a;
	}

	// Genere
	public char getGenere() {
		return this.genere;
	}

	// Edat
	public int getEdat() {
		return this.edat;
	}

	// Pes
	public int getPes() {
		return this.pes;
	}

	// Vacunat
	public boolean getVacunat() {
		return this.vacunat;
	}

	// Chip ID
	public int getChipID() {
		return this.chipID;
	}

}
