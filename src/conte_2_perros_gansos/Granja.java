package conte_2_perros_gansos;

import java.util.ArrayList;

public class Granja {

	private ArrayList<Animal> animals;
	private int numAnimal;
	private int topAnimal;

	//
	// CONSTRUCTORS
	//

	// Defecte
	Granja() {
		animals = new ArrayList<Animal>();
		numAnimal = 0;
		topAnimal = 100;
	}

	// Granja dimensions
	Granja(int dimensions) {
		// Entre 1 i 100
		if (dimensions < 1 || dimensions > 100)
			dimensions = 100;

		topAnimal = dimensions;
		animals = new ArrayList<Animal>();
	}

	//
	// METODES
	//

	public int afegir(Animal anim) {

		// Comprova si te espai
		if (numAnimal >= topAnimal)
			return -1;

		animals.add(anim);
		numAnimal++;

		return numAnimal;
	}

	public boolean treure(Animal anim) {

		if (!animals.remove(anim))
			return false;

		numAnimal--;

		return true;
	}

	public boolean moure(int posicio, Granja gr) {

		// Comprova la posicio tindra gos
		if (posicio >= numAnimal)
			return false;

		Animal anim = this.obtenirAnimal(posicio);

		animals.remove(posicio);
		this.numAnimal--;

		gr.afegir(anim);
		gr.numAnimal++;

		return true;
	}

	@Override
	public String toString() {
		// Mostra l'ocupat del total
		return "Animals: " + numAnimal + "/" + topAnimal;
	}

	public void visualitzar() {
		System.out.println("Num animals: " + getNumAnimals());
		
		for(Animal anim : animals) {	
			if(anim instanceof Ganso)
				System.out.println("- " + (Ganso)anim);
			else if(anim instanceof Gos)
				System.out.println("- " + (Gos)anim);
		}
	}

	public void visualitzarVius() {
		for(Animal anim : animals) {
			if(anim.estat != GosEstat.VIU)
				continue;
			
			if(anim instanceof Ganso)
				System.out.println("- " + (Ganso)anim);
			else if(anim instanceof Gos)
				System.out.println("- " + (Gos)anim);
		}
	}

	// GENERAR GRANJA
	public static Granja generarGranja(int top) {

		Granja gr = new Granja(top);

		// GENERAR CARACTERISTIQUES DE GOSSOS

		// Generar noms
		String[] noms = { "Bethoven", "Jako", "Rex", "Bonie", "Max", "Jack", "Nina", "Chester", "Bobbie", "Zeus",
				"Blacky", "Day", "Melanie", "Wachimichu", "Luna", "Marlos", "Tommy", "Clifford", "Excalibur",
				"Franchesco", "Chelle" };

		// Generar races
		Ra�a[] GosRaces = { new Ra�a("Doberman", GosMida.MITJA, 12), new Ra�a("Chihuahua", GosMida.PETIT, 9),
				new Ra�a("Pastor alemany", GosMida.GRAN, 12), new Ra�a("Caniche", GosMida.PETIT, 8),
				new Ra�a("Labrador", GosMida.MITJA, 11), new Ra�a("Galgo", GosMida.MITJA, 12),
				new Ra�a("Mastin", GosMida.MITJA, 11), new Ra�a("Rottweiler", GosMida.MITJA, 10),
				new Ra�a("Corgi", GosMida.MITJA), new Ra�a("Bull terrier", GosMida.MITJA), null // Sense ra�a
		};

		Animal anim;

		if ((int) (Math.random() * 2) == 0) {

			// GENERA GANSO
			anim = new Ganso();
			anim.edat = (int) (Math.random() * 6 + 1);

		} else {

			// GENERA GOS
			anim = new Gos();
			anim.edat = (int) (Math.random() * 10 + 1);

		}

		
		// Nombre
		anim.nom = noms[(int) (Math.random() * noms.length)];
		
		// Sexe
		if ((int) (Math.random() * 2) == 0)
			anim.sexe = 'M';
		else
			anim.sexe = 'F';

		
		gr.afegir(anim);
		
	return gr;

	}

	public Gos obtenirGos(int index) {
		try {
			return (Gos)(animals.get(index));
		} catch (IndexOutOfBoundsException ext) {
			return null;
		}
	}
	
	public Animal obtenirAnimal(int index) {
		try {
			return animals.get(index);
		} catch (IndexOutOfBoundsException ext) {
			return null;
		}
	}

	public int numTipusAnimal(Class<?> tipusAnimal) {
		
		int num = 0;
		
		//Per cada animal
		for(Animal anim : animals) {

			//Si es del tipus, comptal
			if (anim.getClass().equals(tipusAnimal))
				num++;
		}
		
		return num;
		
	}
	
	//
	// GETTERS & SETTERS
	//

	public ArrayList<Animal> getAnimals() {
		return animals;
	}

	public int getNumAnimals() {
		return numAnimal;
	}

	public int getTopAnimals() {
		return topAnimal;
	}

}
