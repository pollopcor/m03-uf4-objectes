package conte_2_perros_gansos;

public class Principal {

	public static void main(String[] args) {

		// Generar animals
		// ES ABSTRACTE => Animal animal1 = new Animal("Animal 1");
		// ES ABSTRACTE => Animal animal2 = new Animal("Animal 2", 15, 'F');
//		Gos gos1 = new Gos("Yako", 8, 'M', 0, new Ra�a("Dalmata", GosMida.GRAN));
//		Gos gos2 = new Gos("Saly", 7, 'F', 1);
//		Ganso ganso1 = new Ganso();
//		Ganso ganso2 = new Ganso("Quackity", 2, GansoTipus.AGRESSIU, 'M');
//
//
//		System.out.println("Gossos:");
//		System.out.println(gos1);
//		System.out.println(gos2);
//		
//		System.out.println("Gansos:");
//		System.out.println(ganso1);
//		System.out.println(ganso2);

		// 2.A.4 Aparellaments
		// aparellarGranjes();

		// 2.B El bien y el mal
		//relacionarse();

		//2.C Gansos vs Gossos
		//GansosvsGossos();
		
	}

	public static void aparellarGranjes() {

		Granja Granja1 = Granja.generarGranja(20);
		Granja Granja2 = Granja.generarGranja(20);
		Granja GranjaNou = new Granja();

		// Aparella Granja1 amb Granja2
		Animal fill;
		for (int i = 0; i < Granja1.getNumAnimals(); i++) {
			fill = Granja1.obtenirAnimal(i).aparellar(Granja2.obtenirAnimal(i));

			if (fill != null)
				GranjaNou.afegir(fill);
		}

		// Mostra les granjes
		System.out.println("------- Granja 1 -------");
		Granja1.visualitzar();
		System.out.println("\n-------- Granja 2 -------");
		Granja2.visualitzar();
		System.out.println("\n-------- Granja nova --------");
		GranjaNou.visualitzar();
	}

	// 2.B El bien y el mal
	public static void relacionarse() {

		Granja Granja1 = Granja.generarGranja(20);
		Granja Granja2 = Granja.generarGranja(20);
		Granja GranjaNou = new Granja();
		Granja GranjaMorts = new Granja();

		// Aparella Granja1 amb Granja2
		Animal a1;
		Animal a2;
		Animal fill;
		for (int i = 0; i < Granja1.getNumAnimals(); i++) {

			a1 = Granja1.obtenirAnimal(i);
			a2 = Granja2.obtenirAnimal(i);

			// Han d'estar vius
			if (a1.estat != GosEstat.VIU || a2.estat != GosEstat.VIU)
				continue;

			// Mateixa especie
			if (a1.getClass() != a2.getClass()) {
				
				saludarse(a1, a2);
				
				if(a1.estat == GosEstat.MORT)
					Granja1.moure(i, GranjaMorts);
				if(a2.estat == GosEstat.MORT)
					Granja2.moure(i, GranjaMorts);
				
				continue;
			}

			// Diferent sexe
			if (a1.sexe == a2.sexe)
				continue;

			fill = a1.aparellar(a2);

			if (fill != null)
				GranjaNou.afegir(fill);
		}
		
		
		//Missatge final
		if(GranjaNou.getNumAnimals() > GranjaMorts.getNumAnimals()) {
			System.out.println("L'amor ha trionfat ");
			
		} else if (GranjaNou.getNumAnimals() < GranjaMorts.getNumAnimals()) {
			System.out.println("Kaos");
		
		} else {
			System.out.println("N'hi ha un equilibri sobre la vida");
		}

	}

	public static void saludarse(Animal a1, Animal a2) {

		if (a1 instanceof Gos && a2 instanceof Ganso) {

			switch (((Ganso) a2).getTipus()) {

			case AGRESSIU:
				// Ganso agressiu i gos femella
				if (a1.sexe == 'F')
					a1.estat = GosEstat.MORT;
				break;

			case DOMESTIC:
				// Ganso domestic i gos dominant
				if (((Gos) a1).getRa�a() != null)
					if (((Gos) a1).getRa�a().getDominant())
						a2.estat = GosEstat.MORT;
				break;

			case DESCONEGUT:
				if (a1.sexe == 'F')
					a2.estat = GosEstat.MORT;
			}

		} else if (a1 instanceof Ganso && a2 instanceof Gos) {

			switch (((Ganso) a1).getTipus()) {

			case AGRESSIU:
				if(a2.edat < 2)
					a2.estat = GosEstat.MORT;
			
			default:
				if(a2.sexe == 'F' && ((Gos)a2).getRa�a() != null)
					a1.estat = GosEstat.MORT;
			}
		}

	}
	
	
	// 2.C Gansos vs Gossos
	public static void GansosvsGossos() {
		
		Granja Granja1 = Granja.generarGranja(20);
		Granja GranjaNou = new Granja();
		
		Animal fill;
		
		int numGossos;
		int numGansos;
		
		do {
			
			//Per cada animal de la granja
			for (int i = 0; i < Granja1.getNumAnimals() - 1; i++) {
				
				if (Granja1.obtenirAnimal(i).estat != GosEstat.VIU || Granja1.obtenirAnimal(i).estat != GosEstat.VIU)
					continue;
				
				fill = Granja1.obtenirAnimal(i).aparellar(Granja1.obtenirAnimal(i+1));

				if (fill != null)
					GranjaNou.afegir(fill);
			}
			
			//Veure quants animals n'hi ha de cada tipus
			numGossos = Granja1.numTipusAnimal(Gos.class);
			numGansos = Granja1.numTipusAnimal(Ganso.class);
			
		} while(Granja1.getNumAnimals() <= 1 || numGossos == 0 && numGansos > 0 || numGossos > 0 && numGansos == 0);
		
	}

}
