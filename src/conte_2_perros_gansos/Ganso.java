package conte_2_perros_gansos;

public class Ganso extends Animal {

	private static int edatTope = 6;
	private GansoTipus tipus;

	//
	// CONSTRUCTORS
	//

	Ganso() {
		super();
		edatTope = 6;
		tipus = GansoTipus.DESCONEGUT;
	}

	Ganso(String nom) {
		super(nom);
		edatTope = 6;
		tipus = GansoTipus.DESCONEGUT;
	}

	Ganso(String nom, int edat, GansoTipus tipus, char sexe) {

		super(nom, edat, sexe);
		this.tipus = tipus;

	}

	//
	// METODES
	//

	// - Bordar
	@Override
	public void so() {
		System.out.println("quack quack");
	}

	@Override
	public Ganso aparellar(Animal a2) {
		return null;
	}

	// - Mostrar informacio
	@Override
	public String toString() {
		// Mostrar nom, si es menys de 14 caracters, s'emplena amb espais fins arribar a
		// 14
		String str = "Nom: " + String.format("%-" + 14 + "s", nom);
		str += "  Edat: " + String.format("%-" + 2 + "s", edat);
		str += "  Sexe:	 " + sexe;

		str += " Tipus: " + tipus;
//		
//		if (GansoTipus != null) {
//			str += " Ra�a: " + String.format("%-" + 15 + "s", ra�a.getNomRa�a());
//			str += " Dominant: ";
//			if (ra�a.getDominant()) {
//				str += " Si";
//			} else {
//				str += "No";
//			}
//		}

		return str;
	}

	//
	// GETTERS AND SETTERS
	//

	public static int getEdatTope() {
		return edatTope;
	}

	public static void setEdatTope(int edatTope) {
		Ganso.edatTope = edatTope;
	}

	public GansoTipus getTipus() {
		return tipus;
	}

	public void setTipus(GansoTipus tipus) {
		this.tipus = tipus;
	}

}
