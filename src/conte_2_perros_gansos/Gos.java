package conte_2_perros_gansos;

import java.util.ArrayList;

public class Gos extends Animal {

	private static int comptador = 0;

	private String nom = "SenseNom";
	private int edat = 4;
	private char sexe = 'M';
	private int fills = 0;
	private Ra�a ra�a;
	private GosEstat estat = GosEstat.VIU;

	private Gos pare = null;
	private Gos mare = null;

	//
	// CONSTRUCTORS
	//

	// - Sense parametres
	public Gos() {
		super();
		comptador++;
	}

	// - Per nom
	public Gos(String nom) {
		super(nom);
	}

	// - Tots els valors
	public Gos(String nom, int edat, char sexe, int fills) {
		super(nom, edat, sexe);
		this.fills = fills;
	}

	// - Tots els valors i ra�a
	public Gos(String nom, int edat, char sexe, int fills, Ra�a ra�a) {
		this(nom, edat, sexe, fills);
		this.setRa�a(ra�a);
	}

	// - Tots els valors, ra�a, pare i mare
	public Gos(String nom, int edat, char sexe, int fills, Ra�a ra�a, Gos pare, Gos mare) {
		this(nom, edat, sexe, fills, ra�a);
		this.setPare(pare);
		this.setMare(mare);
	}

	// - Copia
	public Gos(Gos gosOriginal) {
		this();
		this.nom = gosOriginal.nom;
		this.edat = gosOriginal.edat;
		this.sexe = gosOriginal.sexe;
		this.fills = gosOriginal.fills;
		this.ra�a = gosOriginal.ra�a;
	}

	// - Clonarse a si mismo
	public Gos clonarme() {
		comptador++;
		Gos GosClonat = new Gos();
		GosClonat.nom = nom;
		GosClonat.edat = edat;
		GosClonat.sexe = sexe;
		GosClonat.fills = fills;
		GosClonat.ra�a = ra�a;

		return GosClonat;
	}

	//
	// METODES
	//

	// - Bordar
	@Override
	public void so() {
		System.out.println("guau guau");
	}

	// - Mostrar informacio
	@Override
	public String toString() {
		// Mostrar nom, si es menys de 14 caracters, s'emplena amb espais fins arribar a
		// 14
		String str = "Nom: " + String.format("%-" + 14 + "s", nom);
		str += "  Edat: " + String.format("%-" + 2 + "s", edat);
		str += "  Sexe: " + sexe + "  Fills: " + fills;
		if (ra�a != null) {
			str += " Ra�a: " + String.format("%-" + 15 + "s", ra�a.getNomRa�a());
			str += " Dominant: ";
			if (ra�a.getDominant()) {
				str += " Si";
			} else {
				str += "No";
			}
		}

		return str;
	}

	// - Copiar perro existente (funcion estatica)
	public static Gos clonarGos(Gos gosOriginal) {

		comptador++;
		Gos gosClonat = gosOriginal;
		return gosClonat;
	}

	// - Numero de gossos creats
	public static void quantitatGossos() {
		System.out.println(comptador);
	}

	// - Aparellar
	@Override
	public Gos aparellar(Animal gos2) {

		if (gos2 == null || gos2 instanceof Gos == false)
			return null;

		if (this.estat != GosEstat.VIU || gos2.estat != GosEstat.VIU)
			return null;

		if (this.sexe == gos2.sexe)
			return null;

		// Generar fill
		Gos fill = new Gos();

		// Edat
		fill.setEdat(0);

		// Sexe
		if ((int) (Math.random() * 2) == 0)
			fill.setSexe('M');
		else
			fill.setSexe('F');

		// Nom
		if (fill.getSexe() == 'M')

			// Pare
			if (this.getSexe() == 'M')
				fill.setNom("Fill de " + this.getNom());
			else
				fill.setNom("Fill de " + gos2.nom);
		else

		// Mare
		if (this.getSexe() == 'F')
			fill.setNom("Fill de " + this.getNom());
		else
			fill.setNom("Fill de " + gos2.nom);

		// Ra�a
		fill.setRa�a(this.getRa�a());

		// Actualitzar num fills dels pares
		this.fills += 1; // (this.fills + 1);
		gos2.fills += 1; // (gos2.fills + 1);

		return fill;

	}

	public void incrementarEdat() {
		edat++;
	}

	public Gos[] pares() {
		return new Gos[] { getPare(), getMare() };
	}

	public ArrayList<Gos> fills(Granja gr) {

		ArrayList<Gos> fills = new ArrayList<Gos>();

		// Per cada gos
		for (Animal anim : gr.getAnimals()) {

			if (anim instanceof Gos) {

				// Comprova si te pares
				if (((Gos) anim).getPare() == null || ((Gos) anim).getMare() == null)
					continue;

				// Mira si es el seu pare o mare
				if (((Gos) anim).getPare().equals(this) || ((Gos) anim).getMare().equals(this))
					// Afegeix-lo com a fill
					fills.add((Gos)anim);
			}
		}

		return fills;
	}

	public boolean germa(Gos gos2) {

		// Si no tenen pares
		if (this.getPare() == null || gos2.getPare() == null)
			return false;

		// Si son el mateix pare o mare
		if (this.getPare().equals(gos2.getPare()) || this.getMare().equals(gos2.getMare()))
			return true;
		else
			return false;
	}

	public ArrayList<Gos> avantPassats() {

		ArrayList<Gos> avpassats = new ArrayList<Gos>();

		// Si no te pares, finalitza
		if (this.getPare() == null || this.getMare() == null) {
			return avpassats;

		} else {

			// Per cada pare
			for (Gos g : this.pares()) {

				// Si no estan afegits, afegeix-los i busca els seus avantpassats
				if (!avpassats.contains(g)) {
					avpassats.add(g);
					avpassats.addAll(g.avantPassats());
				}
			}
		}

		return avpassats;
	}

	// public ArrayList<Gos> comuns(Granja Gr, Gos gos2) {
	//
	// //Guardara els comuns
	// ArrayList<Gos> pasatsComuns = new ArrayList<Gos>();
	//
	// //Guarda els avantpassats meus
	// ArrayList<Gos> passatsMeus = this.avantPassats(Gr);
	//
	// //Per cada avantpassat del Gos2, comprova si jo tamb� el tinc
	// for(Gos g : gos2.avantPassats(Gr)) {
	//
	// if(passatsMeus.contains(g))
	// pasatsComuns.add(g);
	// }
	//
	// return pasatsComuns;
	// }

	public ArrayList<Gos> comuns(Gos gos2) {

		// Guardara els comuns
		ArrayList<Gos> pasatsComuns = new ArrayList<Gos>();

		// Guarda els avantpassats meus
		ArrayList<Gos> passatsMeus = this.avantPassats();

		// Per cada avantpassat del Gos2, comprova si jo tamb� el tinc
		for (Gos g : gos2.avantPassats()) {

			if (passatsMeus.contains(g))
				pasatsComuns.add(g);
		}

		return pasatsComuns;
	}

	// Avantpassats m�s antics (m�s edat)
	public ArrayList<Gos> mesAntic() {

		ArrayList<Gos> avpassats = this.avantPassats();

		switch (avpassats.size()) {

		case 0:
			return null;

		case 1:
			return avpassats;

		default:
			ArrayList<Gos> antics = new ArrayList<Gos>();

			int max_edat = 0;

			// Troba la edat m�s alta (mes vell)
			for (Gos g : avpassats) {
				if (g.getEdat() > max_edat)
					max_edat = g.getEdat();
			}

			// Agafa tots els gossos que tinguin aquesta edat
			for (Gos g : avpassats) {

				// Si es dels mes vells
				if (g.getEdat() == max_edat)
					antics.add(g);
			}

			return antics;
		}
	}

	//
	// GETERS & SETTERS
	//
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {

		if (ra�a == null && edat >= 10) // Sense ra�a
			estat = GosEstat.MORT;
		else if (ra�a != null && edat >= ra�a.getTempsVida()) // Amb ra�a
			estat = GosEstat.MORT;

		this.edat = edat;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public Ra�a getRa�a() {
		return ra�a;
	}

	public void setRa�a(Ra�a ra�a) {
		this.ra�a = ra�a;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

	public Gos getPare() {
		return pare;
	}

	public void setPare(Gos g) {
		this.pare = g;
	}

	public Gos getMare() {
		return mare;
	}

	public void setMare(Gos g) {
		this.mare = g;
	}
}
