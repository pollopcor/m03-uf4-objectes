package conte_2_perros_gansos;

 abstract public class Animal {

	protected String nom;
	protected int edat;
	protected char sexe;
	protected int fills;
	protected GosEstat estat;

	
	//
	// CONSTRUCTORS
	//
	
	public Animal() {
		
	}
	
	public Animal(String nom) {
		this.nom = nom;
	}
	
	public Animal(String nom, int edat, char sexe) {
		this(nom);
		this.edat = edat;
		this.sexe = sexe;
	}
	
	
	
	//
	// METODES
	//
	abstract  void so();
	
	abstract Animal aparellar(Animal a2);

	@Override
	public String toString() {
		return "[Animal] Nom: " + nom + " Edat: " + edat + " Sexe: " + sexe + " Fills: " + fills;	 	
	}
	
//	//
//	// GETTERS & SETTERS
//	//
//	public String getNom() {
//		return this.nom;
//	}
//	public void setNom(String nom) {
//		this.nom = nom;
//	}
//	public int getEdat() {
//		return this.edat;
//	}
//	public void setEdat(int edat) {
//		this.edat = edat;
//	}
//	public char getSexe() {
//		return this.sexe;
//	}
//	public void setSexe(char sexe) {
//		this.sexe = sexe;
//	}
//	public int getFills() {
//		return this.fills;
//	}
//	public void setFills(int fills) {
//		this.fills = fills;
//	}
}
