package celestino;

public class Ra�a {

	private String NomRa�a;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;

	// CONSTRUCTORS
	Ra�a(String nom, GosMida mida, int tempsVida) {
		this(nom, mida);
		this.tempsVida = tempsVida;
	}

	Ra�a(String nom, GosMida gt) {
		this.NomRa�a = nom;
		this.mida = gt;
		this.tempsVida = 10;
		this.dominant = false;
	}
	//Amb dominant
	Ra�a(String nom, GosMida gt, boolean domin) {
		this.NomRa�a = nom;
		this.mida = gt;
		this.tempsVida = 10;
		this.dominant = domin;
	}

	// METODES
	@Override
	public String toString() {

		String str = "Ra�a: " + NomRa�a + " Mida: " + mida + " Temps vida: " + tempsVida + " Dominant: ";
		
		if (dominant)
			str += "Si";
		else
			str += "No";
		
		return str;
	}

	//GETTERS AND SETTERS
	public String getNomRa�a() {
		return NomRa�a;
	}

	public void setNomRa�a(String nomRa�a) {
		NomRa�a = nomRa�a;
	}

	public GosMida getMida() {
		return mida;
	}

	public void setMida(GosMida mida) {
		this.mida = mida;
	}

	public int getTempsVida() {
		return tempsVida;
	}

	public void setTempsVida(int tempsVida) {
		this.tempsVida = tempsVida;
	}

	public boolean getDominant() {
		return dominant;
	}

	public void setDominant(boolean dominant) {
		this.dominant = dominant;
	}
}