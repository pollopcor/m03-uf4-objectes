package celestino;

public class TEST_avantpassats {

	public static void main(String[] args) {
		
		System.out.println("\n\n\n========= AVANTPASSATS ==========");
//		ArrayList<Gos> avant = Gr1.getGossos().get(1).avantPassats(Gr1);
//		for(Gos gz : avant)
//			System.out.println(gz);
		
		Ra�a bulldog = new Ra�a("Bulldog", GosMida.MITJA, 12);
		
		//Avis part de la mare
		Gos gzmare_mare = new Gos("2.1 Mare de la mare", 10, 'M', 1, bulldog);
		Gos gzmare_pare = new Gos("2.1 Pare de la mare", 9, 'M', 1, bulldog);

		//Avis part del pare
		Gos gzpare_mare = new Gos("2.1 Mare del pare", 7, 'F', 1, bulldog);
		Gos gzpare_pare = new Gos("2.1 Pare del Pare", 10, 'M', 1, bulldog);
		
		//Fill, pare i mare
		Gos gzmare = new Gos("1.2 Mare", 6, 'F', 1, bulldog, gzmare_pare, gzmare_mare);
		Gos gzpare = new Gos("1.1 Pare", 5, 'M', 1, bulldog, gzpare_pare, gzpare_mare);
		Gos gzfill = new Gos("1 Fill", 2, 'M', 1, bulldog, gzpare, gzmare);
		
		Gos gzfilla = new Gos("1 Filla", 3, 'M', 1, bulldog, gzpare, gzmare);
		Gos gzAltre = new Gos("Altre", 5, 'M', 1, bulldog);
		
		Granja gr = new Granja();
		gr.afegir(gzfill);
		gr.afegir(gzpare);
		gr.afegir(gzmare);
		gr.afegir(gzpare_pare);
		gr.afegir(gzpare_mare);
		gr.afegir(gzmare_pare);
		gr.afegir(gzmare_mare);
		
		// Avantpassats
		for(Gos av : gzfill.avantPassats())
			System.out.println(av);
		
		// Avantpassats en comu
//		for(Gos av : gzfill.comuns(gr, gzfilla))
//			System.out.println(av);
		
		
		// Avantpassats m�s vells
//		for(Gos av : gzfill.mesAntic(gzfill.avantPassats(gr))) {
//			System.out.println(av);
//		}
		
		
		//Sortir
		boolean a = true;
		if(a)
			return;
		//----------------------------------

	}

}
