package celestino;

import java.util.ArrayList;

public class Celestino {

	public static void main(String[] args) {

		Granja Gr1 = Granja.generarGranja(12);
		Granja Cementiri = new Granja();

		Gos g1;
		Gos g2;
		Gos g3;

		int errades = 0;

		for (int i = 0; i < 10; i++) {

			errades = 0;
			do {

				g1 = Gr1.obtenirGos(i);
				g2 = Gr1.obtenirGos(i + 1);
				
				//Comprova si els gossos existeixen (el primer)
				if(g1 != null)
					g3 = g1.aparellar(g2);
				else
					g3 = null;

				if (g3 == null)
					errades++;
				else
					Gr1.afegir(g3);

			} while (errades < 5);

			// Incrementar edat dels gossos
			for (int j = 0; j < Gr1.getNumGossos(); j++) {
				Gr1.obtenirGos(j).setEdat(Gr1.obtenirGos(j).getEdat() + 1);
			}

			// Treure gossos morts
			for (int j = 0; j < Gr1.getNumGossos(); j++)
				if (Gr1.obtenirGos(j).getEstat() == GosEstat.MORT) {

					Gos g = Gr1.obtenirGos(j);
					
					Gr1.treure(g);
					Cementiri.afegir(g);
				}

			// Mostrar granja
			System.out.println("===== Granja 1 =====");
			Gr1.visualitzar();

			// Mostrar cementiri
			System.out.println("\n===== Cementiri =====");
			Cementiri.visualitzar();
			
			
			System.out.println("\n\n");
			
		}
		
		
		// Mostrar granja evolucionada
		System.out.println("\n===== Granja evolucionada =====\n");
		//Gr1.visualitzarVius();

		// Per cada gos
		for(Gos g : Gr1.getGossos()) {
			
			//Mostra el gos i els pares
			System.out.println(g);
			
			//Comprova que tenen pares
			if(g.getPare() != null && g.getMare() != null) {
				System.out.println(" - Pare: " + g.getPare().getNom());
				System.out.println(" - Mare: " + g.getMare().getNom());
			} else
				System.out.println(" - Progenitors desconeguts");
			
			//Mostra els fills
			ArrayList<Gos> fills = g.fills(Gr1);
			
			if(fills.size() == 0)
				System.out.println(" - Sense fills");
			else {
				
				System.out.print(" - Fills: ");
				for(Gos fill : fills) {
					System.out.print(fill.getNom() + ", ");
				}		
			}
		}
		
		
	}

}
