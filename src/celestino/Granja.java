package celestino;

import java.util.ArrayList;

public class Granja {

	private ArrayList<Gos> gossos;
	private int numGossos;
	private int topGossos;

	//
	// CONSTRUCTORS
	//

	// Defecte
	Granja() {
		gossos = new ArrayList<Gos>();
		numGossos = 0;
		topGossos = 100;
	}

	// Granja dimensions
	Granja(int dimensions) {
		// Entre 1 i 100
		if (dimensions < 1 || dimensions > 100)
			dimensions = 100;

		topGossos = dimensions;
		gossos = new ArrayList<Gos>();
	}

	//
	// METODES
	//

	public int afegir(Gos g) {
		
		// Comprova si te espai
		if (numGossos >= topGossos)
			return -1;

		gossos.add(g);
		numGossos++;

		return numGossos;
	}
	
	public boolean treure(Gos g) {
		
		if(!gossos.remove(g))
			return false;
		
		numGossos--;

		return true;
	}

	public boolean moure(int posicio, Granja gr) {
		
		// Comprova la posicio tindra gos
		if (posicio >= numGossos)
			return false;
		
		Gos g = this.obtenirGos(posicio);

		gossos.remove(posicio);
		this.numGossos--;
		
		gr.afegir(g);
		gr.numGossos++;

		return true;
	}
	
	@Override
	public String toString() {
		// Mostra l'ocupat del total
		return "Gossos: " + numGossos + "/" + topGossos;
	}

	public void visualitzar() {
		System.out.println("Num gossos: " + getNumGossos());
		for (int i = 0; i < getNumGossos(); i++)
			System.out.println("- " + gossos.get(i));
	}

	public void visualitzarVius() {
		for (int i = 0; i < numGossos; i++)
			if (gossos.get(i).getEstat() == GosEstat.VIU)
				System.out.println("- " + gossos.get(i));
	}

	//GENERAR GRANJA
	public static Granja generarGranja(int top) {

		Granja gr = new Granja(top);
		
		// GENERAR GOSSOS
		
		// Generar noms
		String[] noms = {
			"Bethoven", "Jako", "Rex", "Bonie", "Max", "Jack", "Nina", "Chester", "Bobbie", "Zeus", "Blacky", "Day", "Melanie",
			"Wachimichu", "Luna", "Marlos", "Tommy", "Clifford", "Excalibur", "Franchesco", "Chelle"
		};
		
		//Generar races
		Ra�a[] races = {
				new Ra�a("Doberman", GosMida.MITJA, 12),
				new Ra�a("Chihuahua", GosMida.PETIT, 9),
				new Ra�a("Pastor alemany", GosMida.GRAN, 12),
				new Ra�a("Caniche", GosMida.PETIT, 8),
				new Ra�a("Labrador", GosMida.MITJA, 11),
				new Ra�a("Galgo", GosMida.MITJA, 12),
				new Ra�a("Mastin", GosMida.MITJA, 11),
				new Ra�a("Rottweiler", GosMida.MITJA, 10),
				new Ra�a("Corgi", GosMida.MITJA),
				new Ra�a("Bull terrier", GosMida.MITJA),
				null //Sense ra�a
				};
		
		Gos g;
		
		//Per cada espai disponible
		for (int i = 0; i < top; i++) {

			//Nova instancia de gos
			g = new Gos();
			
			// Nombre
			g.setNom(noms[(int)(Math.random() * noms.length)] + " " + (i+1));;
			
			// Ra�a
			g.setRa�a(races[(int)(Math.random() * races.length)]);
			
			// Edat
			int t;
			if(g.getRa�a() == null)
				t = 10;
			else
				t = g.getRa�a().getTempsVida() - 1;
			
			g.setEdat((int)(Math.random() * t + 1));
			
			
			// Sexe
			if((int)(Math.random() * 2) == 0)
				g.setSexe('M');
			else
				g.setSexe('F');

			// Dominant
			if(g.getRa�a() != null)
				if((int)(Math.random() * 2) == 0)
					g.getRa�a().setDominant(true);
				else
					g.getRa�a().setDominant(false);

			// Fills i Estat ja venen per defecte
			
			// AFEGIR GOS
			gr.afegir(g);
			
		}
		
		return gr;
	}

	
	public Gos obtenirGos(int index) {
		try {
			return gossos.get(index);
		} catch (IndexOutOfBoundsException ext) {
			return null;
		}
	}
	
	//
	// GETTERS & SETTERS
	//

	public ArrayList<Gos> getGossos() {
		return gossos;
	}

	public int getNumGossos() {
		return numGossos;
	}

	public int getTopGossos() {
		return topGossos;
	}

	//
	//
	//
}
