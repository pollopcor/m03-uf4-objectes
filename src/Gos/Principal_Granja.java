package Gos;

public class Principal_Granja {

	public static void main(String[] args) {

//		Granja G1 = new Granja(100);
//		Granja G2;
//		Granja G3;
//		
//		// Generar granjes
//		G1 = Granja.generarGranja(5);
//		G2 = Granja.generarGranja(5);
//		G3 = new Granja();
//		
//		
//		G1.visualitzar();
//		System.out.println("-------------------------");
//		G2.visualitzar();
//		System.out.println("-------------------------");
//		G3.visualitzar();
		
		creuarGranjes();
		
	}

	
	public static void creuarGranjes()	{
		
		// Generar granjes
		Granja G1 = Granja.generarGranja(5);
		Granja G2 = Granja.generarGranja(5);
		Granja G3 = new Granja();
		
		Gos fill;
		
		// Per cada gos a la Granja 1
		for(int i=0;i<G1.getNumGossos();i++) {
			
			//Intentar aparellar
			fill = G1.obtenirGos(i).aparellar(G2.obtenirGos(i));
			
			//Si han pogut tenir un fill
			if(fill != null)
				//Afegir fill a la tercera granja
				G3.afegir(fill);
		}
		
		System.out.println("\n====== Granja 1 ======");
		G1.visualitzar();
		
		System.out.println("\n====== Granja 2 ======");
		G2.visualitzar();
		
		System.out.println("\n====== Granja 3 ======");
		G3.visualitzar();
	}	
	
}