package Gos;

public class Principal {

	public static void main(String[] args) {

		// Generar ra�es
		Ra�a bulldog = new Ra�a("Bulldog", GosMida.MITJA, 12);
		Ra�a dalmata = new Ra�a("Dalmata", GosMida.GRAN, 14);

		// Generar gossos
		Gos archi = new Gos("Archi", 5, 'M', 2);	// Tots els parametres
		Gos baco = new Gos("Baco"); 				// Nomes nom
		Gos layssa = new Gos(archi); 				// Copiar un gos
		Gos rex = Gos.clonarGos(archi); 			// Clonar dalmata
		Gos jako = baco.clonarme(); 				// Clonar
		Gos kora = new Gos("Kora", 6, 'F', 1, dalmata); // Amb ra�a

		// Mostrar informacio dels gossos
		System.out.println(archi);
		System.out.println(baco);
		System.out.println(layssa);
		System.out.println(rex);
		System.out.println(kora);

		System.out.println("\n");

		// Fer bordar els gossos
		archi.bordar();
		baco.bordar();
		layssa.bordar();
		rex.bordar();
		jako.bordar();
		kora.bordar();
		
		//Mostra quantitat de gossos
		Gos.quantitatGossos();

	}

}
