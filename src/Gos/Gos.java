package Gos;

public class Gos {

	private static int comptador = 0;

	private String nom = "SenseNom";
	private int edat = 4;
	private char sexe = 'M';
	private int fills = 0;
	private Ra�a ra�a;
	private GosEstat estat = GosEstat.VIU;

	//
	// CONSTRUCTORS
	//

	// - Sense parametres
	public Gos() {
		comptador++;
	}

	// - Per nom
	public Gos(String nom) {
		this();
		this.nom = nom;
		// this.edat = 4;
		// this.sexe = 'M';
		// this.fills = 0;
	}

	// - Tots els valors
	public Gos(String nom, int edat, char sexe, int fills) {
		this(nom);
		this.edat = edat;
		this.sexe = sexe;
		this.fills = fills;
	}

	// - Tots els valors i ra�a
	public Gos(String nom, int edat, char sexe, int fills, Ra�a ra�a) {
		this(nom, edat, sexe, fills);
		this.ra�a = ra�a;
	}

	// - Copia
	public Gos(Gos gosOriginal) {
		this();
		this.nom = gosOriginal.nom;
		this.edat = gosOriginal.edat;
		this.sexe = gosOriginal.sexe;
		this.fills = gosOriginal.fills;
		this.ra�a = gosOriginal.ra�a;
	}

	// - Clonarse a si mismo
	public Gos clonarme() {
		comptador++;
		Gos GosClonat = new Gos();
		GosClonat.nom = nom;
		GosClonat.edat = edat;
		GosClonat.sexe = sexe;
		GosClonat.fills = fills;
		GosClonat.ra�a = ra�a;

		return GosClonat;
	}

	//
	// METODES
	//

	// - Bordar
	public void bordar() {
		System.out.println("guau guau");
	}

	// - Mostrar informacio
	@Override
	public String toString() {
		// Mostrar nom, si es menys de 14 caracters, s'emplena amb espais fins arribar a
		// 14
		String str = "Nom: " + String.format("%-" + 14 + "s", nom);
		str += "  Edat: " + String.format("%-" + 2 + "s", edat);
		str += "  Sexe: " + sexe + "  Fills: " + fills;
		if (ra�a != null) {
			str += " Ra�a: " + String.format("%-" + 15 + "s", ra�a.getNomRa�a());
			str += " Dominant: ";
			if(ra�a.getDominant()) {str += " Si";} else {str += "No";}
		}
		
		return str;
	}

	// - Copiar perro existente (funcion estatica)
	public static Gos clonarGos(Gos gosOriginal) {

		comptador++;
		Gos gosClonat = gosOriginal;
		return gosClonat;
	}

	// - Numero de gossos creats
	public static void quantitatGossos() {
		System.out.println(comptador);
	}

	// - Aparellar
	public Gos aparellar(Gos gos2) {

		if (potAparellarse(gos2)) {

			// Generar fill
			Gos fill = new Gos();

			// Edat
			fill.setEdat(0);
			
			// Sexe
			if ((int) (Math.random() * 2) == 0)
				fill.setSexe('M');
			else
				fill.setSexe('F');

			// Nom
			if(fill.getSexe() == 'M')
				//Pare
				if(this.getSexe() == 'M')
					fill.setNom("Fill de " + this.getNom());
				else
					fill.setNom("Fill de " + gos2.getNom());
			else
				//Mare
				if(this.getSexe() == 'F')
					fill.setNom("Fill de " + this.getNom());
				else
					fill.setNom("Fill de "+ gos2.getNom());

			// Ra�a
			fill.setRa�a(calculaRa�aDominant(this.getRa�a(), gos2.getRa�a()));

			
			// Actualitzar pares
			this.setFills(this.getFills() + 1);
			gos2.setFills(gos2.getFills() + 1);
			
			return fill;
			
		} else
			return null;

	}

	// - Comprovar si pot aparellarse
	private boolean potAparellarse(Gos gos2) {

		// Edat (2 a 10 anys)
		if (this.getEdat() < 2 || this.getEdat() > 10)
			return false;

		// Mascle i Femella
		if (this.getSexe() == 'F' && gos2.getSexe() == 'F' || this.getSexe() == 'M' && gos2.getSexe() == 'M')
			return false;

		// Estan vius
		if (this.getEstat() != GosEstat.VIU || gos2.getEstat() != GosEstat.VIU)
			return false;

		// 3 fills maxim
		if (this.getSexe() == 'F' && this.getFills() > 3 || gos2.getSexe() == 'F' && gos2.getFills() > 3)
			return false;

		// Tamanys compatibles
		if (this.getSexe() == 'M')
			// - Pasa com a mascle
			return tamanyCompatible(this, gos2);
		else
			// - Pasa com a femella
			return tamanyCompatible(gos2, this);

	}

	// VALIDAR TAMANY PARELLA
	private boolean tamanyCompatible(Gos Mascle, Gos Femella) {

		//Comprovar si t� ra�a (per poder comprovar la mida)
		if(Mascle.getRa�a() == null || Femella.getRa�a() == null)
			return false;
		
		// PETIT
		// - Sempre podra aparearse
		if (Mascle.ra�a.getMida() == GosMida.PETIT)
			return true;

		// MITJA
		// - Amb mitjans o grans
		if (Mascle.ra�a.getMida() == GosMida.MITJA) {
			switch (Femella.ra�a.getMida()) {
			case MITJA:
			case GRAN:
				return true;
			default:
				return false;
			}
		}

		// GRAN
		// - Nomes amb els grans
		if (Mascle.ra�a.getMida() == GosMida.GRAN && Femella.ra�a.getMida() == GosMida.GRAN)
			return true;
		else
			return false;
	}

	private Ra�a calculaRa�aDominant(Ra�a mascle, Ra�a femella) {

		// Comprova les ra�es i retorna la important
		if (mascle == null && femella == null)
			return null;
		else if (mascle == null)
			return femella;
		else if (femella == null)
			return mascle;

//		// MASCLE dominant
//		if (mascle.getDominant()) {
//
//			// FEMELLA tamb� dominant
//			if (femella.getDominant())
//				return femella;
//			else
//				return mascle;
//
//		} else
//			// MASCLE no dominant, sempre agafa femella
//			return femella;
		
		if(mascle.getDominant() && !femella.getDominant())
			return mascle;
		else
			return femella;

	}

	//
	// GETERS & SETTERS
	//
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {

		if (ra�a == null && edat >= 10) // Sense ra�a
			estat = GosEstat.MORT;
		else if (ra�a != null && edat >= ra�a.getTempsVida()) // Amb ra�a
			estat = GosEstat.MORT;

		this.edat = edat;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public Ra�a getRa�a() {
		return ra�a;
	}

	public void setRa�a(Ra�a ra�a) {
		this.ra�a = ra�a;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

}
